# Seven Languages in Seven Weeks
This repo contains code for exercises and simple projects implemented while reading the book [Seven Languages in Seven Weeks](https://pragprog.com/titles/btlang/seven-languages-in-seven-weeks/)
