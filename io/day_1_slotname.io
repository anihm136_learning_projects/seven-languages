repl := Object clone

repl greet := method("Hi! Welcome to simple greeter!" println)
repl encourage := method("You're doing a great job! Keep it up!" println)
repl exit := method(doMessage(break))
repl handleUnknown := method("That action cannot be performed. Please try again" println)

repl greet
loop(
	m := File standardInput readLine
	if(repl getSlot(m), repl getSlot(m) call, repl handleUnknown)
)
