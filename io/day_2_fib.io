Fibgen := Object clone
Fibgen prev := 1
Fibgen cur := 1
Fibgen generate := method(length, if(
	length<=2,
		self cur,
	tmp := self cur
	self cur = self cur + self prev
	self prev = tmp
	self generate(length-1)
	)
)
Fibgen generate_loop := method(length, 
	for(i, 3, length,
		tmp := self cur
		self cur = self cur + self prev
		self prev = tmp
	)
	self cur
)

for(i, 1, 10, Fibgen clone generate(i) print; " " print; Fibgen clone generate_loop(i) println)
0
