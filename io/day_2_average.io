List myAverage := method(
	if(self size == 0, 0,
		self sum / self size
	)
)

list(1,2,3,4,5) myAverage println
list() myAverage println
