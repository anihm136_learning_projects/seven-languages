Number origDiv := Number getSlot("/")

Number / = method(denom, if(denom==0, 0, origDiv(denom)))

(10/2) println
(0/2) println
(10/0) println
(0/0) println
