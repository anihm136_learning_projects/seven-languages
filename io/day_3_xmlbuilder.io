Builder := Object clone
Builder level := 0
Builder forward := method(
  for(i, 1, self level, write("    "))
  writeln("<", call message name, ">")
  
  self level = self level + 1
  call message arguments foreach(
	arg, 
	content := self doMessage(arg)
	if(content type == "Sequence", for(i, 1, self level, write("    ")); writeln(content)))
  self level = self level - 1
  
  for(i, 1, self level, write("    "))
  writeln("</", call message name, ">"))
Builder  ul(
	li("Io"), 
	li("Lua"), 
	li("JavaScript"))
