Range
Matrix := Object clone
Matrix data := list()
Matrix isTransposed := false

Matrix dim := method(x, y, self data = List clone preallocateToSize(y); 1 to(y) foreach(_, self data push(1 to(x) asList)); self)
Matrix get := method(x, y, if(self isTransposed, self data at(x) at(y), self data at(y) at(x)))
Matrix set := method(x, y, value, if(self isTransposed, self data at(x) atPut(y, value), self data at(y) atPut(x, value)))

Matrix transpose := method(self isTransposed = self isTransposed not; self)

matrix := Matrix clone
matrix dim(2, 3) println
matrix get(0,0) println
matrix get(0,1) println
matrix get(1,2) println
matrix set(0,0, 100)
matrix set(0,1, 200)
matrix set(1,2,300)
matrix println
matrix transpose println
0
