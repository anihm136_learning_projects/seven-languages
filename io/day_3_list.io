squareBrackets := method(
	l := list()
	call message arguments foreach(arg,
		content := self doMessage(arg)
		l push(content)
	)
	l
)

a := [1,2,3,[1,2,3]]
a println
