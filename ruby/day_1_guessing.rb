target = rand(10)

puts 'Guess the number (between 0 and 9)'

guess = -1
until guess == target
  puts 'Enter your guess: '
  raw_guess = gets
  guess = raw_guess.to_i
  if guess != target
    puts "That's the wrong number! Your guess was too #{guess < target ? 'low' : 'high'}"
  else
    puts 'That\'s right! Congratulations!'
  end
end
