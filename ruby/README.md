# Week 1: Ruby
- Day 1: Implement a simple guessing game
- Day 2:
    - Writing a class to file
    - Working with trees
    - Writing a simple grep program
- Day 3: working with CSV, method_missing
- Project: Simple UDP server from scratch
