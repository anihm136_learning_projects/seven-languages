module ActsAsCsv
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def acts_as_csv
      include InstanceMethods
    end
  end

  module InstanceMethods
    attr_accessor :headers, :csv_contents

    def initialize
      read
    end
    
    def read
      # filename = self.class.to_s.downcase + '.csv'
      filename = "/dev/stdin"
      file = File.new(filename)

      @headers = file.gets.chomp.split(', ')
      @csv_contents = []
      file.each do |row|
        @csv_contents << row.chomp.split(', ')
      end
    end

    def each(&block)
      @csv_contents.each {|row| block.call(CsvRow.new(@headers, row))}
    end
  end

  class CsvRow
    attr_accessor :headers, :row
    
    def initialize(headers, row)
      @headers = headers
      @row = row
    end

    def method_missing(name, *args)
      @row[@headers.find_index(name.to_s)]
    end
  end
end

class RubyCsv
  include ActsAsCsv
  acts_as_csv
end

# m = RubyCsv.new
# puts m.headers.inspect
# puts m.csv_contents.inspect

csv = RubyCsv.new
csv.each {|row| puts row.one}
