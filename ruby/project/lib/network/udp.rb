require_relative 'util'

module Network
  module UDP
    class Datagram
      attr_reader :bytes

      include NetworkUtil

      def initialize(bytes)
        @bytes = bytes
      end

      def source_port
        word16(bytes[0], bytes[1])
      end

      def dest_port
        word16(bytes[2], bytes[3])
      end

      def length
        word16(bytes[4], bytes[5])
      end

      def checksum
        word16(bytes[6], bytes[7])
      end

      def body
        bytes[8..].pack('C*')
      end

      def to_s
        {
          source_port:,
          dest_port:,
          length:,
          body:
        }.to_s
      end
    end
  end
end
