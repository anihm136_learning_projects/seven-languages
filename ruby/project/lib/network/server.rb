require 'socket'

module Network
  class Server
    def initialize(interface)
      interface_idx = get_interface_index(interface)
      @socket = open_ethernet_socket(interface_idx)
    end

    def listen_and_serve(address, port)
      res_socket = UDPSocket.new
      res_socket.bind(address, port)

      loop do
        data = @socket.recv(BUFFER_SIZE)

        frame = Network::Ethernet::Frame.new(data.bytes)
        next unless frame.ethertype == Network::Ethernet::ETHERTYPE_IP

        packet = Network::IP::Packet.new(frame.payload)
        next unless packet.protocol == Network::IP::PROTOCOL_UDP && packet.dest_ip == address

        datagram = Network::UDP::Datagram.new(packet.payload)
        next unless datagram.dest_port == port

        response = yield datagram.body
        res_socket.send(
          response, 0, packet.source_ip, datagram.source_port
        )
      end
    end

    BUFFER_SIZE = 1024

    private

    # Send an ioctl request using a raw (ethernet) socket to get the index of the interface to listen on
    # We send a request containing a binary sequence equivalent to the following struct -
    # struct ifreq {
    #     char ifr_name[IFNAMSIZ]; /* Interface name */ -- this is 36b
    #     union {
    #         struct sockaddr ifr_addr;
    #         struct sockaddr ifr_dstaddr;
    #         struct sockaddr ifr_broadaddr;
    #         struct sockaddr ifr_netmask;
    #         struct sockaddr ifr_hwaddr;
    #         short           ifr_flags;
    #         int             ifr_ifindex; -- this is 4b
    #         int             ifr_metric;
    #         int             ifr_mtu;
    #         struct ifmap    ifr_map;
    #         char            ifr_slave[IFNAMSIZ];
    #         char            ifr_newname[IFNAMSIZ];
    #         char           *ifr_data;
    #     };
    # };
    # We populate ifr_name and send the request - the ioctl populates the union field ifr_ifindex with the index of the device if it exists
    def get_interface_index(interface)
      socket = Socket.open(:PACKET, :RAW)

      ifreq_struct = [interface].pack("a#{IFREQ_SIZE}")
      socket.ioctl(SIOCGIFINDEX, ifreq_struct)

      ifreq_struct[Socket::IFNAMSIZ, IFINDEX_SIZE] # Return bytes 37-40 from the response
    end

    IFREQ_SIZE = 0x0028 # Size of the request struct in C
    IFINDEX_SIZE = 0x0004 # Size of the index field
    SIOCGIFINDEX = 0x8933 # ioctl operation code for retrieving device index

    # To open a socket to receive raw frames from the network device, we need to bind the socket to it. This is done by passing a binary sequence equivalent to the following struct -
    # struct sockaddr_ll {
    #     unsigned short sll_family;   /* Always AF_PACKET */
    #     unsigned short sll_protocol; /* Physical-layer protocol */
    #     int            sll_ifindex;  /* Interface number */
    #     unsigned short sll_hatype;   /* ARP hardware type */ -- blank
    #     unsigned char  sll_pkttype;  /* Packet type */ -- blank
    #     unsigned char  sll_halen;    /* Length of address */ -- blanck
    #     unsigned char  sll_addr[8];  /* Physical-layer address */ -- blank
    # };
    def open_ethernet_socket(interface_idx)
      socket = Socket.open(:PACKET, :RAW)

      sockaddr_ll = [Socket::AF_PACKET].pack('s')
      sockaddr_ll << [ETH_P_ALL].pack('s')
      sockaddr_ll << interface_idx
      sockaddr_ll << ("\x00" * (SOCKADDR_LL_SIZE - sockaddr_ll.length))

      socket.bind(sockaddr_ll)

      socket
    end

    SOCKADDR_LL_SIZE = 0x0014 # Size of the sockaddr_ll struct in C
    ETH_P_ALL = 0x0300 # Code to receive every packet
  end
end
