module NetworkUtil
  private
  
  def word16(lbyte, rbyte)
    (lbyte << 8) | rbyte
  end
end
