require_relative 'util'

module Network
  module Ethernet
    ETHERTYPE_IP = 0x0800

    class Frame
      attr_reader :bytes

      include NetworkUtil

      def initialize(bytes)
        @bytes = bytes
      end

      def destination_mac
        format_mac(bytes[0, 6])
      end

      def source_mac
        format_mac(bytes[6, 6])
      end

      def ethertype
        word16(bytes[12], bytes[13])
      end

      def payload
        bytes[14..]
      end

      # In Linux, crc is filtered out at physical layer
      # def crc
      #   bytes[-4..]
      # end

      def to_s
        {
          source: source_mac,
          destination: destination_mac,
          frame_size: bytes.size,
          payload_size: payload.size,
          ethertype:
        }.to_s
      end

      private

      def format_mac(mac_bytes)
        mac_bytes.map do |byte|
          byte.to_s(16).rjust(2, '0')
        end.join(':').upcase
      end
    end
  end
end
