require_relative 'util'

module Network
  module IP
    PROTOCOL_UDP = 0x11

    class Packet
      attr_reader :bytes

      include NetworkUtil

      def initialize(bytes)
        @bytes = bytes
      end

      def version
        bytes[0] >> 4
      end

      def header_length
        (bytes[0] & 0b00001111) * 4
      end

      def type_of_service
        bytes[1]
      end

      def datagram_length
        word16(bytes[2], bytes[3])
      end

      def identification
        word16(bytes[4], bytes[5])
      end

      def flags; end

      def fragment_offset; end

      def ttl
        bytes[8]
      end

      def protocol
        bytes[9]
      end

      def header_checksum
        word16(bytes[10], bytes[11])
      end

      def source_ip
        bytes[12, 4].join('.')
      end

      def dest_ip
        bytes[16, 4].join('.')
      end

      def options
        bytes[20, header_length - 20]
      end

      def payload
        bytes[header_length..]
      end

      def to_s
        {
          version:,
          header_length:,
          datagram_length:,
          identification:,
          protocol:,
          source_ip:,
          dest_ip:,
          options:,
          payload_size: payload.size
        }.to_s
      end
    end
  end
end
