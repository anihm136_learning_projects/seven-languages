require 'socket'

BUFFER_SIZE = 1024

socket = UDPSocket.new
socket.bind('localhost', 9999)

loop do
  message, sender = socket.recvfrom(BUFFER_SIZE)

  p sender
  port = sender[1]
  host = sender[2]

  socket.send(message.upcase, 0, host, port)
end
