require_relative 'lib/network'

INTERFACE_NAME = 'veth1'.freeze
ADDRESS = '192.168.100.100'.freeze
PORT = 9999

server = Network::Server.new(INTERFACE_NAME)

server.listen_and_serve(ADDRESS, PORT, &:upcase) # { |data| data.upcase }
