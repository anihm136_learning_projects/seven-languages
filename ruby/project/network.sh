#! /usr/bin/env bash

set -euxo pipefail

server_link="veth1"
server_namespace="server"
server_ip="192.168.100.100/24"
host_link="veth2"
host_namespace="host"
host_ip="192.168.100.101/24"

setup() {
	# Create virtual ethernet pair
	sudo ip link add "$server_link" type veth peer name "$host_link" 

	# Set up server in its own namespace
	sudo ip netns add "$server_namespace"
	sudo ip link set dev "$server_link" netns "$server_namespace"
	sudo ip -n "$server_namespace" address add "$server_ip" dev "$server_link"
	sudo ip -n "$server_namespace" link set "$server_link" up

	# Set up host in its own namespace
	sudo ip netns add "$host_namespace"
	sudo ip link set dev "$host_link" netns "$host_namespace"
	sudo ip -n "$host_namespace" address add "$host_ip" dev "$host_link"
	sudo ip -n "$host_namespace" link set "$host_link" up
}

teardown() {
	sudo ip netns delete "$server_namespace"
	sudo ip netns delete "$host_namespace"
}

server() {
	sudo ip netns exec "$server_namespace" ruby udpserver_raw.rb
}

client_ping() {
	sudo ip netns exec "$host_namespace" ping -c 3 "${server_ip%/*}"
}

client_udp() {
	sudo ip netns exec "$host_namespace" sh -c "echo 'Hello world' | nc -u ${server_ip%/*} 9999"
}

[[ "$1" == "setup" ]] && setup && exit 0
[[ "$1" == "teardown" ]] && teardown && exit 0
[[ "$1" == "server" ]] && server && exit 0
[[ "$1" == "client_ping" ]] && client_ping && exit 0
[[ "$1" == "client_udp" ]] && client_udp && exit 0
