class Tree
  attr_accessor :children, :node_name

  def initialize(name, children=[])
    @children = children
    @node_name = name
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end

  def visit(&block)
    block.call self
  end
end

my_tree = Tree.new("Ruby",
                   [Tree.new("Reia"), Tree.new("MacRuby")])

puts 'Visiting the root node'
my_tree.visit {|n| puts n.node_name}

puts 'Visiting the entire tree'
my_tree.visit_all {|n| puts n.node_name}

# Assignment
class EnhancedTree
  attr_accessor :children, :node_name

  def initialize(initializer={})
    if initializer.empty? then return end
    @children = []
    init_array = initializer.to_a
    @node_name = init_array[0][0]
    init_array[0][1].each {|child| @children.push(EnhancedTree.new([child]))}
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end

  def visit(&block)
    block.call self
  end
end

my_enhanced_tree = EnhancedTree.new( {'grandpa' => { 'dad' => {'child 1' => {}, 'child 2' => {} }, 'uncle' => {'child 3' => {}, 'child 4' => {} } } } )
puts 'Visiting the root node'
my_enhanced_tree.visit {|n| puts n.node_name}

puts 'Visiting the entire tree'
my_enhanced_tree.visit_all {|n| puts n.node_name}
