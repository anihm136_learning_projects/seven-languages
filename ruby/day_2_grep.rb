puts "Enter the file to search: "
filepath = gets.chomp
puts "Enter the pattern to search for: "
pattern = gets.chomp

File.foreach(File.expand_path(filepath)).with_index {|line, i| puts "#{i} #{line.chomp}" if line =~ /#{pattern}/}
